// extension的Tab页
var commonTab = Vue.component('common-tab', {
    template: readCommonHTML("commonTab"),
    props: {
        type: {
            type: String
        }
    },
    data() {
        return {
            tree: {
                defaultData:{
                    dir:"加载中",
                    item:""
                },
                nodes: []
            },
            tab: {
                defaultData:{
                    label:"加载中",
                    data: {}
                },
                selectTab:"",
                tabs: [],
            }
        }
    },
    mounted() {
        if (!this.type) {
            this.type = this.$route.params.type;
        }
        this.init(this.type);
    },
    methods: {
        init(type) {
            var that = this;
            this.tab.tabs = [];
            axios.get('/simpledevelop/common/tab/get/?type=' +  type).then(function (resp) {
                var data = resp.data.data;
                that.tree.defaultData.dir = data.defaultTreeDir;
                that.tree.defaultData.item = data.defaultTreeItem;
                that.tree.nodes = data.nodes;
                that.tab.defaultData.label = data.defaultTabLabel;
                that.tab.selectTab = data.defaultTabLabel;
            });
        },
        openJointTemplate(label, name) {
            var bool = false;
            var that = this;
            this.tab.tabs.forEach((tab, index) => {
                if (tab.label === label) {
                    that.tab.selectTab = tab.label;
                    bool = true;
                    return;
                }
            });
            this.tab.selectTab = label;
            if (bool || label === this.tab.defaultData.label) {
                return;
            }
            this.tab.tabs.push({
                label : label,
                name : name
            })
        },
        handleTabsEdit(targetName, action) {
            if (action === 'remove') {
                let tabs = this.tab.tabs;
                let activeName = this.tab.selectTab;
                if (activeName === targetName) {
                    tabs.forEach((tab, index) => {
                        if (tab.label === targetName) {
                            let nextTab = tabs[index + 1] || tabs[index - 1];
                            if (nextTab) {
                                activeName = nextTab.label;
                            }
                        }
                    });
                }
                this.tab.selectTab = activeName;
                this.tab.tabs = tabs.filter(tab => tab.label !== targetName);
                if (this.tab.tabs.length == 0) {
                    this.tab.selectTab = this.tab.defaultData.label;
                }
            }
        }
    },
    watch: {
        // 利用watch方法检测路由变化：
        $route: function(to, from) {
            if (to.path !== from.path) {
                this.type = this.$route.params.type;
                this.init(to.params.type);
            }
        }
    }
})