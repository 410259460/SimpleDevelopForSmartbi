Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        titles: [],
        projectPath: '',
        loading: false,
        changeGlobal: false
    },
    mutations: {
        changeTitles(state, titles) {
            state.titles = titles
        },
        changePath(state, path) {
            state.projectPath = path;
            var separator = anLogin.$data.separator
            if (path.indexOf(separator + "src" + separator + "web") != -1) {
                state.projectPath = path.substring(0, path.indexOf(separator + "src" + separator + "web"));
            }
        },
        changeLoading(state, status) {
            state.loading = status;
        },
        changeGlobal(state, status) {
            state.changeGlobal = status;
        }
    }
})

Vue.prototype.getExtendJson = function (path, name) {
    axios.get('/simpledevelop/template/custom/extend/json?path=' + path + '&name=' + name).then(resp => this.ruleForm.extendJson = resp.data.data);
}

Vue.prototype.openWindowFile = function (path) {
    axios.get('/simpledevelop/util/openFile?path=' + path);
}

// 后台cmd复制内容
Vue.prototype.copyContent = function (content) {
    axios.get('/simpledevelop/util/copyContent?content=' + content);
    this.$message({
        message: '内容已复制到粘贴板上',
        type: 'success'
    });
}
// 前端监听某个按钮点击复制 开源库clipboard
Vue.prototype.setCopyElem = function (className) {
    var that = this;
    var clipboard = new ClipboardJS(className);
    //成功回调
    clipboard.on('success', function(e) {
        that.$message({
            message: '内容已复制到粘贴板上',
            type: 'success'
        });
        e.clearSelection();
    });
    //失败回调
    clipboard.on('error', function(e) {
        that.$message({
            message: '内容复制失败',
            type: 'error'
        });
    });
}
// 前端直接复制内容 开源库clipboard
Vue.prototype.setCopyText = function (text, e) {
    var that = this;
    var clipboard = new ClipboardJS(e.target, {
        text: () => text
    });
    //成功回调
    clipboard.on('success', function(e) {
        that.$message({
            message: '内容已复制到粘贴板上',
            type: 'success'
        });
        e.clearSelection();
        clipboard.destroy()
    });
    //失败回调
    clipboard.on('error', function(e) {
        that.$message({
            message: '内容复制失败',
            type: 'error'
        });
        clipboard.destroy()
    });
    clipboard.onClick(e)
}

Vue.prototype.openLoading = function () {
    this.$store.commit('changeLoading', true);
}
Vue.prototype.closeLoading = function () {
    this.$store.commit('changeLoading', false);
}

Vue.prototype.delsrcweb = function (path) {
    if (path.indexOf(this.separator + "src" + this.separator + "web")) {
        path = path.substring(0, path.lastIndexOf(this.separator + "src" + this.separator + "web"))
    }
    return path;
}
var anLogin = new Vue({
    el: '#app',
    data() {
        return {
            separator: ""
        }
    },
    store: store,
    mounted() {
        if (!this.$route.path || this.$route.path === '' || this.$route.path === '/') {
            this.$router.push({path: '/buildConfig'});
        }
        axios
            .get('/simpledevelop/util/file/separator')
            .then(resp => this.separator = resp.data);
    },
    methods: {},
    router: new VueRouter({
        // mode: 'history', // 去掉url中的#
        routes: [
            {path: '/buildConfig', component: buildConfig},
            {path: '/language', component: language},
            {path: '/extensions/:title', component: extensions},
            {path: '/upgrade', component: upgrade},
            {path: '/managerExtension', component: managerExtension},
            {path: '/loadExtensions', component: loadExtensions},
            {path: '/extensionTab', component: extensionTab},
            {path: '/apiSearch', component: apiSearch},
            {path: '/apiTab', component: apiTab},
            {path: '/jointTemplate', component: jointTemplate},
            {path: '/commonTab/:type', component: commonTab},
        ]
    })
})
