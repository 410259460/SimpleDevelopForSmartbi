// 加载指定目录扩展包
var loadExtensions = Vue.component('load-extensions', {
    template: readComponentHTML("loadExtensions"),
    data() {
        return {
            ruleForm: {
                path: '',
                type: '',
            },
            rules: {
                path: [
                    {required: true, message: '请输入文件夹地址', trigger: 'blur'}
                ]
            }
        };
    },
    mounted() {
        this.ruleForm.type = 'append';
    },
    methods: {
        // 新增标签
        submitForm: function () {
            var that = this;
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    that.openLoading();
                    var postData = new URLSearchParams()
                    postData.append("type", this.ruleForm.type);
                    postData.append("path", this.ruleForm.path);
                    axios.post(
                        "/simpledevelop/extension/loadExtensions",
                        postData
                    ).then(function (response) {
                        if (response.data.reasonCode == 2000) {
                            that.$message({
                                message: '更新成功',
                                type: 'success'
                            });
                            that.$store.commit('changeGlobal', true);
                        } else {
                            this.$message.error(response.data.desc);
                        }
                        that.closeLoading();
                    }).catch(error => this.$message.error(JSON.stringify(error)))
                } else {
                    return false;
                }
            });
        }
    }
})