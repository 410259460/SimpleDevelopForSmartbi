// 管理extension.list标签
var managerExtension = Vue.component('manager-extension', {
    template: readComponentHTML("managerExtension"),
    data() {
        return {
            form: {
                title: ''
            },
            ruleForm: {
                title: '',
                path: ''
            },
            oldTitle: '',
            oldPath: '',
            result: '结果展示',
            isLocation: true,
            dialogFormVisible: false,
            separator: '',
            rules: {
                title: [
                    {required: true, message: '请输入标签名', trigger: 'blur'},
                ],
                path: [
                    {required: true, message: '请输入smartbi地址', trigger: 'blur'}
                ]
            }
        };
    },
    mounted() {
        this.init();
        this.separator = this.$root.$data.separator;
    },
    methods: {
        modify() {
            if (this.oldTitle !== this.ruleForm.title || this.oldPath !== this.ruleForm.path) {
                var that = this;
                axios
                    .get('/simpledevelop/extension/modifyTitle?oldTitle=' + this.oldTitle + '&title=' + this.ruleForm.title + '&path=' + this.ruleForm.path)
                    .then(function (response) {
                        that.$message({
                            message: '修改成功',
                            type: 'success'
                        });
                        that.dialogFormVisible = false;
                        that.init();
                    })
                    .catch(error => this.$message.error(JSON.stringify(error)));
                return;
            }
            this.$message({
                message: '请修改数据后再提交',
                type: 'warning'
            });
        },
        handleEdit(index, row) {
            this.dialogFormVisible = true;
            this.oldTitle = row.title;
            this.oldPath = row.path;
            this.ruleForm.title = row.title;
            this.ruleForm.path = row.path;
        },
        handleDelete(index, row) {
            var that = this;
            axios
                .get('/simpledevelop/extension/deleteTitle?title=' + row.title)
                .then(function (response) {
                    that.$message({
                        message: '删除成功',
                        type: 'success'
                    });
                    that.init();
                })
                .catch(error => this.$message.error(JSON.stringify(error)));
        },
        // 新增标签
        submitForm: function () {
            var that = this;
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    // if (this.ruleForm.path.indexOf(this.separator + "WEB-INF" + this.separator + "extensions" + this.separator + "extensions.list") == -1) {
                    //     this.ruleForm.path += this.separator + "WEB-INF" + this.separator + "extensions" + this.separator + "extensions.list";
                    // }
                    that.openLoading();
                    axios
                        .get('/simpledevelop/extension/addTitle?title=' + this.ruleForm.title + "&path=" + this.ruleForm.path)
                        .then(function (response) {
                            if (response.data === "该值已存在") {
                                that.$message.error(response.data);
                            } else if (response.data === "文件不存在") {
                                that.$message.error(response.data);
                            } else {
                                that.$message({
                                    message: '新增标签成功',
                                    type: 'success'
                                });
                            }
                            that.init();
                            that.closeLoading();
                        })
                        .catch(error => this.$message.error(JSON.stringify(error)));
                } else {
                    that.$message.error('请完善必填信息');
                    return false;
                }
            });
        },
        // 重置
        resetForm: function () {
            this.$refs['ruleForm'].resetFields();
        },
        // 初始化 读取全局的标签集合
        init: function () {
            var that = this;
            that.openLoading();
            axios.get('/simpledevelop/extension/getTitles')
                .then(function (response) {
                    if (response.data) {
                        titles = [];
                        for (var key in response.data) {
                            titles.push({
                                title: key,
                                path: response.data[key]
                            })
                        }
                        that.$store.commit('changeTitles', titles);
                    }
                    that.closeLoading();
                });
        }
    }
})