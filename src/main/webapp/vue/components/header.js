// 导航栏
var header = Vue.component('simple-header', {
    template: readComponentHTML("header"),
    // props: {
    //     activeIndex: null
    // },
    data() {
        return {
            // option的value 对应 key的值
            key: {},
            options: [],
            optionsCopy: [],
            value: '',
            separator: '',
            activeIndex: null,
            searchTimeout: null,
            isSearch: false,
            repeatCheck: null
        }
    },
    mounted() {
        this.initPathSelect();
        this.activeIndex = this.$route.path;
        if (!this.activeIndex || this.activeIndex === '' || this.activeIndex === '/') {
            this.activeIndex = '/buildConfig';
        }
        axios
            .get('/simpledevelop/util/file/separator')
            .then(resp => this.separator = resp.data);
    },
    methods: {
        del(item, e) {
            for (var i in this.options) {
                if (this.options[i].path === item) {
                    this.key[item] = false;
                    this.options.splice(i, 1);
                    axios.post(
                        "/simpledevelop/extension/updateGlobalHistory",
                        this.options
                    ).then(ret => this.$message({
                        message: '删除记录成功',
                        type: 'success'
                    })).catch(error => this.$message.error(JSON.stringify(error)))
                }
            }
            for (var i in this.optionsCopy) {
                if (this.optionsCopy[i].path === item) {
                    this.optionsCopy.splice(i, 1);
                }
            }
        },
        initPathSelect() {
            var that = this;
            axios
                .get('/simpledevelop/extension/getGlobalHistory')
                .then(function (response) {
                    if (response.data && response.data.length > 0) {
                        that.options = response.data;
                        that.optionsCopy = that.options;
                        that.key = {};
                        for (var i in that.options) {
                            that.key[that.options[i].path] = true;
                        }
                    }
                })
                .catch(error => this.$message.error(JSON.stringify(error)));
            that.$store.commit('changeGlobal', false);
        },
        addSpace(e){ // 由于el-select 的空格被拦截了，手动监听空格健添加空格
            this.value += " ";
        },
        changePath(item) {
            if (!item) {
                return;
            }
            var oldItem = item;
            if (item.indexOf(this.separator + "src" + this.separator + "web") == -1) {
                if (oldItem.lastIndexOf(this.separator) != -1) {
                    oldItem = oldItem.substring(oldItem.lastIndexOf(this.separator) + 1);
                } else {
                    oldItem = item;
                }
                item += this.separator + "src" + this.separator + "web";
            } else {
                oldItem = oldItem.substring(0, oldItem.indexOf(this.separator + "src" + this.separator + "web"));
                if (oldItem.lastIndexOf(this.separator) != -1) {
                    oldItem = oldItem.substring(oldItem.lastIndexOf(this.separator) + 1);
                } else {
                    oldItem = item;
                }
            }
            var newOption = null;
            if (!this.key[item]) {
                this.key[item] = true;
                newOption = {
                    path: item,
                    shortName: oldItem
                };
                this.options.push(newOption);
                axios.post(
                    "/simpledevelop/extension/updateGlobalHistory",
                    this.options
                ).then(ret => this.$message({
                    message: '新增扩展包成功',
                    type: 'success'
                })).catch(error => this.$message.error(JSON.stringify(error)))
            }
            this.$store.commit('changePath', item);
            if (newOption) {
                return newOption;
            }
        },
        filter(val){
            this.value = val;
        },
        dataFilter(val) {
            this.value = val
            if (val) {
                var _this = this;
                clearTimeout(this.searchTimeout);
                this.searchTimeout = setTimeout(function (){
                    _this.openLoading();
                    var postData = new URLSearchParams();
                    postData.append("search", _this.value);
                    axios.post(
                        "/simpledevelop/extension/searchGlobalHistory",
                        postData
                    ).then(function (ret) {
                        _this.closeLoading();
                        _this.optionsCopy = ret.data;
                        if (_this.optionsCopy.length == 0 && val.indexOf(_this.separator + "src" + _this.separator + "web") != -1) {
                            var newOpention = _this.changePath(val);
                            if (newOpention) {
                                _this.optionsCopy.push(newOpention);
                                _this.value = newOpention.shortName;
                            }
                        }
                    })
                        .catch(error => _this.$message.error(JSON.stringify(error)))
                }, 500)
            } else { //val为空时，还原数组
                this.optionsCopy = this.options;
            }
        }
    },
    computed: {
        listenChangeGlobalPaths() {
            return this.$store.state.changeGlobal;
        }
    },
    watch: {
        listenChangeGlobalPaths: function (newValue, oldValue) {
            if (newValue) {
                this.initPathSelect();
            }
        }
    }
})