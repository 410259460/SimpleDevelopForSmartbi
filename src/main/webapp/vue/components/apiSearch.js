// copyright
var apiSearch = Vue.component('api-search', {
    template: readComponentHTML("apiSearch"),
    props: {
        isMethod: {
            type: Boolean,
            default: false,
        },
        path: {
            type: String
        },
        type: {
            type: Number
        }
    },
    data() {
        return {
            ruleForm: {
                filed: '',
                configs: [],
            },
            list: [],
            count: 0,
            searchAll: true,
            columnCount: "2",
            currentPage: 1
        };
    },
    mounted() {
        if (!this.isMethod) {
            this.ruleForm.configs = ["js", "java", "2"];
        }
        this.searchData(1);
    },
    methods: {
        test: function (path, type) {
            if (!this.isMethod) {
                var name = path.slice(path.lastIndexOf(".") + 1) + ((type === 1) ? ".java" : ".js");
                this.$emit("addMethodTab", {name: name, title: name, isMethod: true, path: path, type: type});
            }
        },
        // 开始国际化
        submitForm: function () {
            this.searchData(1);
        },
        searchData: function (page) {
            var that = this;
            this.openLoading();
            var postData = new URLSearchParams();
            postData.append("word", this.ruleForm.filed);
            postData.append("page", page);
            postData.append("column", this.columnCount);
            var method = "";
            if (!this.isMethod) {
                postData.append("configs", this.ruleForm.configs);
                method = "searchClass";
            }
            if (this.isMethod) {
                postData.append("path", this.path);
                postData.append("type", this.type);
                method = "searchMethod";
            }
            axios
                .post(
                    '/simpledevelop/api/' + method,
                    postData
                ).then(function (response) {
                that.$message({
                    message: '查询完成',
                    type: 'success'
                });
                that.searchAll = (!that.ruleForm.filed);
                that.currentPage = page;
                that.list = response.data.data.data;
                that.count = response.data.data.count;
                that.closeLoading();
            }).catch(error => this.$message.error(JSON.stringify(error)));
        },
        pageChange: function (count) {
            this.searchData(count);
        },
        change: function () {
            this.searchData(1);
        }
    }
})