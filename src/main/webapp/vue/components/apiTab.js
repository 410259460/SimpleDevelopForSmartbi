// extension的Tab页
var apiTab = Vue.component('api-tab', {
    template: readComponentHTML("apiTab"),
    data() {
        return {
            title: '新建查询',
            name: '1',
            isMethod: false,
            path: "",
            editableTabsValue: '1',
            editableTabs: [],
            tabIndex: 1
        }
    },
    methods: {
        addMethodTab(obj) {
            for (var i = 0; i < this.editableTabs.length; i++) {
                if (this.editableTabs[i].name === obj.name) {
                    this.$message.error("该标签已存在");
                    return;
                }
            }
            this.tabIndex++;
            this.editableTabs.push(obj);
            this.editableTabsValue = obj.name;
        },
        handleTabsEdit(targetName, action) {
            if (action === 'add') {
                let newTabName = ++this.tabIndex + '';
                this.editableTabs.push({
                    title: '新建查询',
                    name: newTabName
                });
                this.editableTabsValue = newTabName;
            }
            if (action === 'remove') {
                let tabs = this.editableTabs;
                let activeName = this.editableTabsValue;
                if (activeName === targetName) {
                    tabs.forEach((tab, index) => {
                        if (tab.name === targetName) {
                            let nextTab = tabs[index + 1] || tabs[index - 1];
                            if (nextTab) {
                                activeName = nextTab.name;
                            }
                        }
                    });
                }

                this.editableTabsValue = activeName;
                this.editableTabs = tabs.filter(tab => tab.name !== targetName);
            }
        }
    }
})