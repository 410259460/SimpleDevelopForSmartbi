function readComponentHTML(file) {
    return readHTML("./components/template/" + file + ".html");
}
function readCommonHTML(file) {
    return readHTML("./common/template/" + file + ".html");
}

function readHTML(file){
    var template = ""
    $.ajax({
        async: false,
        url : file,
        success : function(result){
            template = result;
        }
    });
    return template;
}