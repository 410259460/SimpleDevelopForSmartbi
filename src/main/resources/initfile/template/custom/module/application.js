<?xml version="1.0" encoding="UTF-8"?> 
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN 2.0//EN" "http://www.springframework.org/dtd/spring-beans-2.0.dtd"> 
<beans> 
    <bean id="framework" class="smartbi.framework.Framework" factory-method="getInstance"> 
        <property name="modules"> 
			<map> 
				<entry><key><value>${fileName}</value></key><ref bean="${fileName}" /></entry>
			</map>
        </property> 
    </bean> 
    <bean id="rmi" class="smartbi.framework.rmi.RMIModule" factory-method="getInstance"> 
        <property name="modules"> 
			<map> 
				<entry><key><value>${fileName}</value></key><ref bean="${fileName}" /></entry>
			</map>
        </property> 
    </bean> 
	<bean id="${fileName}" class="${package}.${fileName}" factory-method="getInstance"></bean>
</beans>
