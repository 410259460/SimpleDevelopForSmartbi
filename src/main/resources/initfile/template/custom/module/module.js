package ${package};

import org.apache.log4j.Logger;
import smartbi.framework.IModule;

public class ${fileName} implements IModule {

    private static Logger LOG = Logger.getLogger(${fileName}.class);
	
	private ${fileName}() {}

    private static ${fileName} instance;

    /**
     * 获取${fileName}单例
     *
     * @return 单例
     */
    public static ${fileName} getInstance() {
        if (instance == null) {
            instance = new ${fileName}();
        }
        return instance;
    }
	
    @Override
    public void activate() {
    }
}