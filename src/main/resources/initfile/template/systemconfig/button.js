<#-- 创建按钮 -->
var AbstractSystemConfigItem = jsloader.resolve("freequery.config.configitem.AbstractSystemConfigItem");
var domutils = jsloader.resolve("freequery.lang.domutils");
var util = jsloader.resolve("freequery.common.util");
var modalWindow = jsloader.resolve("freequery.common.modalWindow");
var dialogFactory = jsloader.resolve("freequery.dialog.dialogFactory");

var ${fileName} = function() {
    this.itemName = "${itemName}"; // 配置项名称
    this.dbkey = "${dbkey}";
};
lang.extend(${fileName}, AbstractSystemConfigItem);

${fileName}.prototype.init = function() {
    this.tr = document.createElement("tr");
    this.tr.height = "30";

    this.td1 = document.createElement("td");
    this.td1.align = "left";
    this.td1.width = "200px";
    this.td1.innerHTML = this.itemName + "${'$'}{Colon}";
    this.tr.appendChild(this.td1);

    this.td2 = document.createElement("td");
    this.td2.innerHTML = "<input class='_${fileName}Btn button-buttonbar-noimage' value='${defaultValue!}' type='button' style='width:100%;'/>";
    this.tr.appendChild(this.td2);

    this.btn = domutils.findElementByClassName([this.tr], "_${fileName}Btn");
    this.addListener(this.btn, "click", this.doBtnClick, this);

    this.td3 = document.createElement("td");
    this.td3.innerHTML = "${description!}";
    this.tr.appendChild(this.td3);

    return this.tr;
}

// 检查配置信息是否合法 
${fileName}.prototype.validate = function() {
    return true;
}

// 保存配置并返回是否保存成功，对于从系统配置表里的获取数据的配置项来说，返回一个对象
${fileName}.prototype.save = function() {
    return true;
}

// 对于从系统配置表里的获取数据的配置项来说，需要在初始化后根据配置信息来显示
${fileName}.prototype.handleConfig = function(systemConfig) {
}

${fileName}.prototype.doBtnClick = function() {
}
