<#-- 创建文本域 -->
var AbstractSystemConfigItem = jsloader.resolve("freequery.config.configitem.AbstractSystemConfigItem");
var domutils = jsloader.resolve("freequery.lang.domutils");
var util = jsloader.resolve("freequery.common.util");
var modalWindow = jsloader.resolve("freequery.common.modalWindow");
var dialogFactory = jsloader.resolve("freequery.dialog.dialogFactory");

var ${fileName} = function() {
    this.itemName = "${itemName}"; // 配置项名称
    this.dbkey = "${dbkey}";
};
lang.extend(${fileName}, AbstractSystemConfigItem);

${fileName}.prototype.init = function() {
    this.tr = document.createElement("tr");
    this.tr.height = "30";

    this.td1 = document.createElement("td");
    this.td1.align = "left";
    this.td1.width = "200px";
    this.td1.innerHTML = this.itemName + "${'$'}{Colon}";
    this.tr.appendChild(this.td1);

    this.td2 = document.createElement("td");
    this.td2.innerHTML = "<textarea class='_${fileName} inputtext' style='width:100%; height:60px;'>${defaultValue!}</textarea>";
    this.tr.appendChild(this.td2);

    this.editer = domutils.findElementByClassName([this.tr], "_${fileName}");

    this.td3 = document.createElement("td");
    this.td3.innerHTML = "${description!}";
    this.tr.appendChild(this.td3);

    return this.tr;
}

// 检查配置信息是否合法 
${fileName}.prototype.validate = function() {
    return true;
}

<#assign langValueOrValue= longValue ? string('longValue', 'value')/>
// 保存配置并返回是否保存成功，对于从系统配置表里的获取数据的配置项来说，返回一个对象
${fileName}.prototype.save = function() {
    if (!this.validate()) {
        return false;
    }
    var value = this.editer.value;
    var obj = {
        key: this.dbkey,
        ${langValueOrValue}: value
    };
    return obj;
}

// 对于从系统配置表里的获取数据的配置项来说，需要在初始化后根据配置信息来显示
${fileName}.prototype.handleConfig = function(systemConfig) {
    for (var i in systemConfig) {
        var config = systemConfig[i];
        if (config && config.key == this.dbkey) {
            var value = systemConfig[i].${langValueOrValue};
            this.editer.value = value;
            break;
        }
    }
}
