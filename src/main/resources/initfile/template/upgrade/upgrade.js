<#-- 创建升级类模板文件 -->
package ${package}.upgrade;

import org.apache.log4j.Logger;
import smartbi.repository.UpgradeHelper;
import smartbi.repository.UpgradeTask;
import smartbi.util.DBType;
import smartbi.util.DbUtil;
import smartbi.util.ValueType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
<#assign newLine= "\r\n                                  "/>
public class UpgradeTask_${nowVersion} extends UpgradeTask {
    private static final Logger LOG = Logger.getLogger(UpgradeTask_${nowVersion}.class);

    @Override
    public boolean doUpgrade(Connection connection, DBType dbType) {
        PreparedStatement pre = null;
        String tableName = "${tableName!}";
        try {
            //判断表是否存在
            if (UpgradeHelper.isTableExists(connection, tableName)) {
                LOG.info("升级类中表已存在");
                return true;
            }

            String sql = UpgradeHelper.getCreateSQL(dbType, tableName,
                    new String[]{<#list fields as field>"${field}"<#if (field_index + 1) % 4 == 0>${newLine}</#if><#if field_has_next>, </#if></#list>},
                    new ValueType[]{<#list types as type>${type}<#if (type_index + 1) % 4 == 0>${newLine}</#if><#if type_has_next>, </#if></#list>},
                    new boolean[]{<#list isNulls as isNull>${isNull}<#if (isNull_index + 1) % 4 == 0>${newLine}</#if><#if isNull_has_next>, </#if></#list>},
                    new String[]{<#list priFields as priField>"${priField}"<#if (priField_index + 1) % 4 == 0>${newLine}</#if><#if priField_has_next>, </#if></#list>});
            pre = connection.prepareStatement(sql);
            pre.execute();
            return true;
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            DbUtil.closeDBObject(null, pre, null);
        }
        return false;
    }

    @Override
    public String getNewVersion() {
        return "${nextVersion}";
    }
	
	<#if !less85>
    @Override
    public String getDate() {
        return "${createDate}";
    }
	</#if>
}
