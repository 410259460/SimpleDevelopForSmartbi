<#-- 创建升级类对应实体类的dao -->
package ${package}.dao;

import ${package}.entity.${entityName};
import smartbi.repository.AbstractDAO;
import smartbi.repository.IDAOModule;
import smartbi.usermanager.UserManagerModule;

public class ${entityName}DAO extends AbstractDAO<${entityName}, String> {

    protected ${entityName}DAO(IDAOModule daoModule) {
        super(daoModule);
    }

    private static ${entityName}DAO _instance = 
            new ${entityName}DAO(UserManagerModule.getInstance().getDaoModule());

    public static ${entityName}DAO getInstance() {
        return _instance;
    }
}