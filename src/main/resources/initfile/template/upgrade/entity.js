<#-- 创建升级类对应实体 -->
package ${package}.entity;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import javax.persistence.Column;
import javax.persistence.Id;
import org.hibernate.annotations.CacheConcurrencyStrategy;
<#if createIdClass>
import javax.persistence.IdClass;
</#if>

@Entity
@Table(name = "${tableName}")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "POJO")
@NamedQueries({})
<#if createIdClass>
@IdClass(${entityName}IdsEntity.class)
</#if>
public class ${entityName} {
	
	<#list fieldMsgs?keys as key>
    private ${fieldMsgs[key].classType} ${fieldMsgs[key].fieldName};
	</#list>
	
	<#list fieldMsgs?keys as key>
	<#list fieldMsgs[key].annotation as annotation>
	${annotation}
	</#list>
	public ${fieldMsgs[key].classType} ${fieldMsgs[key].methodGet}() {
		return this.${fieldMsgs[key].fieldName};
	}
	
	public void ${fieldMsgs[key].methodSet}(${fieldMsgs[key].classType} ${fieldMsgs[key].fieldName}) {
		this.${fieldMsgs[key].fieldName} = ${fieldMsgs[key].fieldName};
	}
	
	</#list>
}
