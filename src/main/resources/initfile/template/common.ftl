<#-- forEach 方法名--list 集合--split 分隔符--number 多少数之后开始换行--char 值左右的包含符--lastAppend 最后的结束符 -->
<#macro forEach list split space number char lastApeend>
<#list list as obj>${char!}${obj}${char!}<#if (obj_index + 1) % number == 0>${"\r\n"}${space}</#if><#if obj_has_next>${split} </#if></#list>${lastApeend!}
</#macro>
