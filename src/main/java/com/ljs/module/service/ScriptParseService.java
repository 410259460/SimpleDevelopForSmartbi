package com.ljs.module.service;

import com.ljs.module.common.DataBean;
import com.ljs.module.common.PathEnum;
import com.ljs.module.entity.template.joint.JointTemplateDto;
import com.ljs.module.service.impl.ExportParse;
import com.ljs.module.service.impl.FunctionParse;
import com.ljs.module.service.impl.LinkParse;
import com.ljs.module.util.FileUtil;
import com.ljs.module.util.TemplateUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ScriptParseService {
    List<IScriptParse> scriptParses = new ArrayList<>();

    public ScriptParseService() {
        scriptParses.add(new LinkParse());
        scriptParses.add(new ExportParse());
        scriptParses.add(new FunctionParse());
    }

    public DataBean compile(JointTemplateDto dto, Map<String, Object> map) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            String content = dto.getLinks() + dto.getTemplateCode();
            String compile = TemplateUtil.renderString(content, map);
            String data = TemplateUtil.removeAnnotation(compile);
            String[] modules = data.split(TemplateUtil.JOINT_TEMPLATE_SPLIT);
            for (String module : modules) {
                String[] scripts = module.split(";");
                for (String script : scripts) {
                    script += ";";
                    for (IScriptParse scriptPars : scriptParses) {
                        if (scriptPars.compile(script)) {
                            DataBean parse = scriptPars.parse(script);
                            stringBuffer.append(script + (parse.getReasonCode() != 2000 ? parse.getDesc() : "解析成功") + "\r\n");
                            break;
                        }
                    }
                }
            }
            return DataBean.success().setDesc(stringBuffer.toString());
        } catch (Exception e) {
            return DataBean.fail().setDesc("解析脚本时出错\r\n" + e.getMessage());
        }
    }

    public DataBean compile(String jointTemplatePath, Map<String, Object> map) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            String content = FileUtil.readFile2Content(PathEnum.JOINT_TEMPLATE + File.separator + jointTemplatePath);
            content = TemplateUtil.removeJsCode(content);
            String compile = TemplateUtil.renderString(content, map);
            // jsCode不参与解析
            String data = TemplateUtil.removeAnnotation(compile);
            String[] modules = data.split(TemplateUtil.JOINT_TEMPLATE_SPLIT);
            for (String module : modules) {
                String[] scripts = module.split(";");
                for (String script : scripts) {
                    script += ";";
                    for (IScriptParse scriptPars : scriptParses) {
                        if (scriptPars.compile(script)) {
                            DataBean parse = scriptPars.parse(script);
                            stringBuffer.append(script + (parse.getReasonCode() != 2000 ? parse.getDesc() : "解析成功") + "\r\n");
                            break;
                        }
                    }
                }
            }
            return DataBean.success().setDesc(stringBuffer.toString());
        } catch (Exception e) {
            return DataBean.fail().setDesc("解析脚本时出错\r\n" + e.getMessage());
        }
    }

    public DataBean action(String jointTemplatePath, Map<String, Object> map) {
        try {
            DataBean compile = compile(jointTemplatePath, map);
            if (compile.getReasonCode() != 2000) {
                return compile;
            }
            map.putAll(((ExportParse)scriptParses.get(1)).getData());
            return scriptParses.get(2).action(map).setDesc(compile.getDesc());
        } catch (Exception e) {
            return DataBean.fail().setData("解析脚本时出错\r\n" + e.getMessage());
        }
    }
}
