package com.ljs.module.service.impl;

import com.ljs.module.common.DataBean;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LinkParse extends AbstractParse<List<String>> {
    List<String> templates = new ArrayList<>();

    @Override
    public String getRegx() {
        return "Link ([\\s\\S]*?);";
    }

    @Override
    public DataBean parse(String data) {
        String template = getMatcher().group(1).trim();
        if (StringUtils.isBlank(template)) {
            return DataBean.fail(data + " 模板不能指定为空");
        }
        templates.add(template);
        return DataBean.success();
    }

    @Override
    public DataBean action(Map<String, Object> map) {
        return null;
    }

    @Override
    public List<String> getData() {
        return templates;
    }
}
