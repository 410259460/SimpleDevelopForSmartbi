package com.ljs.module.service;

import com.ljs.module.common.DataBean;

import java.util.List;
import java.util.Map;

public interface IScriptParse<T> {

    boolean compile(String data);

    DataBean parse(String data);

    DataBean action(Map<String, Object> map);

    T getData();

    List<String> getScripts();
}
