package com.ljs.module.util;


import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lijunsai on 2021/07/19
 */
public class Util {

    public static String appendLanguage(String str) {
        return "${" + str + "}";
    }

    /**
     * 升级类版本比较
     * @param v1 旧版本
     * @param v2 旧版本
     * @return 新版本
     */
    public static String versionCompare(String v1, String v2) {
        String[] splitV1 = v1.split("_");
        String[] splitV2 = v2.split("_");
        for (int i = 0; i < 3; i++) {
            String s1 = splitV1[i];
            String s2 = splitV2[i];
            if (s1.equals(s2)) {
                continue;
            }
            int i1 = Integer.parseInt(s1);
            int i2 = Integer.parseInt(s2);
            if (i1 > i2) {
                return v1;
            }
            return v2;
        }
        return null;
    }

    /**
     * 升级类版本排序
     * @param v1 旧版本
     * @param v2 旧版本
     * @return 版本相减结果
     */
    public static int versionSort(String v1, String v2) {
        String[] splitV1 = v1.split("_");
        String[] splitV2 = v2.split("_");
        for (int i = 0; i < 3; i++) {
            String s1 = splitV1[i];
            String s2 = splitV2[i];
            if (s1.equals(s2)) {
                continue;
            }
            return Integer.parseInt(s1) - Integer.parseInt(s2);
        }
        return 0;
    }

    /**
     * 表字段转实体字段 以下滑线区分驼峰
     * @param field 表字段
     * @return
     */
    public static String tableFiled2EntityField(String field){
        if (field.indexOf("c_") == 0) {
            field = field.substring(2);
        }
        String[] s = field.split("_");
        String ret = s[0];
        for (int i = 1; i < s.length; i++) {
            ret += (s[i].charAt(0) + "").toUpperCase() + s[i].substring(1);
        }
        return ret;
    }

    /**
     * 升级类 表字段中如果存在 | 则以|区分
     * @param str 表字段
     * @return
     */
    public static String[] containThenSplit(String str){
        str = toLowerCase(str.trim());
        if (str.contains("|")) {
            String[] split = str.split("\\|");
            split[0] = split[0].trim();
            split[1] = split[1].trim();
            return split;
        } else {
            return new String[]{str, Util.tableFiled2EntityField(str)};
        }
    }

    /**
     * 升级类表字段部分全部小写
     * @param str 表字段
     * @return
     */
    public static String toLowerCase(String str) {
        if (str.contains("|")) {
            String[] split = str.split("\\|");
            return toSublineAndLowerCase(split[0]) + "|" + split[1];
        } else {
            return toSublineAndLowerCase(str);
        }
    }

    /**
     * 将大写转 _小写  例如 N 转成 _n (部分驼峰转下划线) 去除连续_
     * @param str
     * @return
     */
    public static String toSublineAndLowerCase(String str) {
        String ret = "";
        char[] chars = str.toCharArray();
        char pre = 't'; // 去除连续_
        for (char aChar : chars) {
            if (pre == '_' && pre == aChar)
                continue;
            ret += (aChar >= 'A' && aChar <= 'Z') ? (((pre == '_') ? "" : "_") + (char)(aChar + 32)) : aChar;
            pre = aChar;
        }
        return ret;
    }

    /**
     * 把文本设置到剪贴板（复制）
     */
    public static void setClipboardString(String text) {
        // 获取系统剪贴板
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        // 封装文本内容
        Transferable trans = new StringSelection(text);
        // 把文本内容设置到系统剪贴板
        clipboard.setContents(trans, null);
    }

    /**
     * 兼容Linux与Window的文件路径
     * @return
     */
    public static String getNowFileSepartor() {
        return File.separator.equals("\\") ? "\\\\" : File.separator;
    }

    public static String replaceFileSeparor(String path) {
        return path.replaceAll("/", getNowFileSepartor());
    }

    public static List<Field> getAllField(Class aClass) {
        List<Field> declaredFields = Arrays.stream(aClass.getDeclaredFields()).collect(Collectors.toList());
        while (!aClass.getSuperclass().toString().equals(Object.class.toString())) {
            aClass = aClass.getSuperclass();
            declaredFields.addAll(Arrays.asList(aClass.getDeclaredFields()));
        }
        return declaredFields;
    }

    public static boolean isNullOrEmpty(String... str){
        for (String s : str) {
            if (StringUtils.isBlank(s)) {
                return true;
            }
        }
        return false;
    }

    public static Object isBoolOrString(String str) {
        if (str.equals("true") || str.equals("false")) {
            return Boolean.valueOf(str);
        } else {
            return str;
        }
    }
}
