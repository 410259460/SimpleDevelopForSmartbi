package com.ljs.module.entity.smartbi;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lijunsai on 2021/08/25
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class XmlBean {
    private String id;
    private String packageName;
    private Map<String, XmlBean> beans;
    @JsonIgnore
    private List<String> beanNames = new ArrayList<>();
    @JsonIgnore
    private List<String> beanRefs = new ArrayList<>();;
}
