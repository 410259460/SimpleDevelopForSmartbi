package com.ljs.module.entity.smartbi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.File;
import java.util.Objects;

/**
 * 扩展包列表实体
 * Created by lijunsai on 2021/07/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Accessors(chain = true)
public class ExtensionsList {
    // 扩展包路径
    private String path;
    private boolean ext;
    // 扩展包简称
    private String shortName;
    // ext同时存在list文件中和文件夹下
    private boolean inFile;

    public ExtensionsList (String path){
        setPath(path);
    }

    public void initPath(String path){
        this.path = path;
    }

    public ExtensionsList setPath(String path){
        path = path.trim();
        this.path = path;
        ext = path.contains(".ext");
        if (!ext) {
            String base = path.substring(0, path.indexOf(File.separator + "src" + File.separator + "web"));
            if (base.lastIndexOf(File.separator) != -1) {
                this.shortName = base.substring(base.lastIndexOf(File.separator) + 1);
            } else {
                this.shortName = base;
            }
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExtensionsList that = (ExtensionsList) o;
        return Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }

    public ExtensionsListSearchDto transDto(){
        return (ExtensionsListSearchDto)new ExtensionsListSearchDto().setPath(this.path).setExt(this.ext)
                .setInFile(this.inFile).setShortName(this.shortName);
    }
}
