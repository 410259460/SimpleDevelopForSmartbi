package com.ljs.module.entity;

import com.ljs.module.annotation.ConfigName;
import com.ljs.module.annotation.TemplateKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 选项开关实体
 * Created by lijunsai on 2021/07/12
 */
@TemplateKey
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfigProperties{
    // 是否注入Language.properties
    @TemplateKey
    @ConfigName("propertis")
    private boolean insertLanguage = false;
    // 是否注入ConfigurationPatch.js
    @TemplateKey
    @ConfigName("config")
    private boolean insertConfigurationPatch = false;
    // 是否以文件名作为前缀
    @TemplateKey
    @ConfigName("prefix")
    private boolean fileNamePrefix = false;
    // 是否带上中文意思
    @TemplateKey
    @ConfigName("cn")
    private boolean appenCN = false;
    // 是否自定义configItem
    @TemplateKey
    @ConfigName("cusConfigItem")
    private boolean cusConfigItem = false;
    // 是否本地模式
    @TemplateKey
    @ConfigName("location")
    private boolean location = false;
    // 重复是否覆盖
    @TemplateKey
    @ConfigName("override")
    private boolean override = false;
    // 是否构建表
    @TemplateKey
    @ConfigName("buildTable")
    private boolean buildTable = false;
    // 是否构建实体
    @TemplateKey
    @ConfigName("buildTableEntity")
    private boolean buildEntity = false;
    // 是否构建实体DAO
    @TemplateKey
    @ConfigName("buildTableEntityDAO")
    private boolean buildEntityDAO = false;
    // 断网模式
    @TemplateKey
    @ConfigName("notInternet")
    private boolean notInternet = false;
    // 禁止ftp缓存
    @TemplateKey
    @ConfigName("noFtpCache")
    private boolean noFtpCache = false;
    // 版本小于等于
    @TemplateKey
    @ConfigName("less85")
    private boolean less85 = false;
    // 是否是longValue
    @TemplateKey
    @ConfigName("longValue")
    private boolean longValue = false;

    private static Map<String, Field> fieldMap = new HashMap<>();

    static {
        Class<ConfigProperties> aClass = ConfigProperties.class;
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field field : declaredFields) {
            ConfigName annotation = field.getAnnotation(ConfigName.class);
            if (annotation != null && !StringUtils.isBlank(annotation.value())) {
                fieldMap.put(annotation.value(), field);
            }
        }
    }

    public static ConfigProperties of (String[] pros) {
        ConfigProperties configProperties = new ConfigProperties();
        if (pros == null || pros.length <= 0)
            return configProperties;
        for (String pro : pros) {
            Field field = fieldMap.get(pro);
            if (field != null) {
                field.setAccessible(true);
                try {
                    field.set(configProperties, true);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return  configProperties;
    }
}
