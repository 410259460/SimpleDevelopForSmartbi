package com.ljs.module.entity.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by lijunsai on 2021/09/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
public class ApiEntity {
    // 0 js  1 java
    private Integer type;
    private String keyword;
    private String father;
    private String desc;
    private String name;
    private String version;
    private String path;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    // 0 没删  1 删除
    private Integer isDelete;

    public ApiEntityDto transDto(){
        return (ApiEntityDto)new ApiEntityDto().setType(this.type).setKeyword(this.keyword)
                .setDesc(this.desc).setName(this.name).setVersion(this.version).setFather(this.father)
                .setPath(this.path).setUpdateTime(this.updateTime).setIsDelete(this.isDelete);
    }
}
