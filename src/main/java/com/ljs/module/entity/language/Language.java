package com.ljs.module.entity.language;

import com.ljs.module.entity.ConfigProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

/**
 * 国际化实体
 * Created by lijunsai on 2021/07/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Language {
    // 最后存放到国际化文件中的KEY值
    private String enToUppercase;
    // 不拼接文件名的enToUppercase
    private String noFileName;
    // 中文
    private String zh;
    // 英文
    private String en;
    // 繁体
    private String tw;
    private String zhUnicode;
    private String enUnicode;
    private String twUnicode;
    // 文件名
    private String fileName;

    /**
     * 转成File文件中的内容
     * @return
     */
    public String toStringFILE() {
        return "\r\n## " + zh + "\r\n"
                + "## 英文:\r\n" + enToUppercase + "=" + enUnicode + "\r\n"
                + "## 简体:\r\n" + enToUppercase + "=" + zhUnicode + "\r\n"
                + "## 繁体:\r\n" + enToUppercase + "=" + twUnicode + "\r\n";
    }

    /**
     * 转成File文件中的内容
     * @return
     */
    public String toStringHTML() {
        return "## " + zh + "<br>"
                + "## 英文:<br>" + enToUppercase + "=" + enUnicode + "<br>"
                + "## 简体:<br>" + enToUppercase + "=" + zhUnicode + "<br>"
                + "## 繁体:<br>" + enToUppercase + "=" + twUnicode + "<br>";
    }

    /**
     * 通过类型转String
     * @param type 类型 en zh tw
     * @return
     */
    public String toStringByType(String type, ConfigProperties configProperties) {
        String str = "";
        if (configProperties.isAppenCN()) {
            str += "## " + zh + "\r\n";
        }
        if (configProperties.isFileNamePrefix()) {
            str += (StringUtils.isBlank(fileName) ? "" : fileName +  "_");
        }
        str += enToUppercase + " = ";
        switch (type) {
            case "en" : str += enUnicode; break;
            case "zh" : str += zhUnicode; break;
            case "tw" : str += twUnicode; break;
        }
        return str;
    }

}
