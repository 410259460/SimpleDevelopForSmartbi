package com.ljs.module.entity.language;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ljs.module.entity.BaseEntity;
import com.ljs.module.util.ConvertrUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Translate extends BaseEntity {
    public Map<String, Language> convertrMap = new HashMap<>();

    /**
     * 将国际化存入文件中
     * @param sb
     * @param fileName 文件名
     * @return
     */
    public StringBuilder mapAppendFile(StringBuilder sb, String fileName){
        StringBuilder[] allPros = getAllPros();
        sb.append(
                "\r\n##英文" + "\r\n##" + fileName + "\r\n" + allPros[0].toString()
                        + "\r\n##简体" + "\r\n##" + fileName + "\r\n" + allPros[1].toString()
                        + "\r\n##繁体" + "\r\n##" + fileName + "\r\n" + allPros[2].toString());
        return sb;
    }

    /**
     * 分别获取国际化中0英文 1简体 2繁体的builder
     * @return
     */
    public StringBuilder[] getAllPros(){
        StringBuilder[] builders = new StringBuilder[] {new StringBuilder(), new StringBuilder(), new StringBuilder()};
        convertrMap.forEach((k, v) -> {
            if (configProperties.isAppenCN()) {
                builders[0].append("##" + v.getZh() + "\r\n");
                builders[1].append("##" + v.getZh() + "\r\n");
                builders[2].append("##" + v.getZh() + "\r\n");
            }
            builders[0].append(v.getEnToUppercase() + " = " + v.getEnUnicode() + "\r\n");
            builders[1].append(v.getEnToUppercase() + " = " + v.getZhUnicode() + "\r\n");
            builders[2].append(v.getEnToUppercase() + " = " + v.getTwUnicode() + "\r\n");
        });
        return builders;
    }

    /**
     *
     * 国际化统一使用该方法，经过这个方法会存储在convertrMap中
     * @param zh
     * @param fileName 文件名
     * @return 国际化实体
     */
    public Language humpString(String zh, String fileName){
        // 遇到 ${} 则不做国际化
        String ret = ConvertrUtil.judgeSpecialZh(zh.trim());
        if (!StringUtils.isBlank(ret)) {
            Language language = new Language()
                    .setEnToUppercase(ret)
                    .setNoFileName(ret)
                    .setEn(ret);
            return language;
        }
        if (convertrMap.containsKey(zh)) {
            return convertrMap.get(zh);
        }
        String str = "";
        if (zh.contains("|")) {
            String[] split = zh.split("\\|");
            str = split[0].trim();
            zh = split[1].trim();
        }
        String en = ConvertrUtil.zhToEn(zh, configProperties)
                .replaceAll(" \\(","(")
                .replaceAll(" \\[","[")
                .replaceAll(" \\{", "{");
        String[] s = en.split(" ");
        String lEn = "";
        for (String s1 : s) {
            lEn += ("" + s1.charAt(0)).toUpperCase() + s1.substring(1).toLowerCase();
        }
        String tw = ConvertrUtil.simpleToComplex(zh);
        // 国际化  如果使用|带了国际化就用这个 没带就用itemName翻译后的
        String key = (StringUtils.isBlank(str) ? en.replaceAll(" ", "_").toUpperCase() : str);
        String noFileName = key;
        if (configProperties.isFileNamePrefix()) {
            key = (StringUtils.isBlank(fileName) ? "" : fileName +  "_") + key;
        }
        Language language = new Language()
                .setEnToUppercase(key)
                .setNoFileName(noFileName)
                .setEn(lEn)
                .setZh(zh)
                .setTw(tw)
                .setEnUnicode(("" + en.charAt(0)).toUpperCase() + en.substring(1))
                .setZhUnicode(ConvertrUtil.encodeUnicode(zh))
                .setTwUnicode(ConvertrUtil.encodeUnicode(tw));

        convertrMap.put(zh, language);
        return language;
    }

    /**
     * 获取国际化命名
     * @param name 原中文
     * @return 国际化命名
     */
    public String getLKey (String name) {
        return convertrMap.get(name).getEnToUppercase();
    }
}
