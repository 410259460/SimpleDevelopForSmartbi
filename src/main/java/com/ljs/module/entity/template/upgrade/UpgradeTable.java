package com.ljs.module.entity.template.upgrade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by lijunsai on 2021/08/16
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpgradeTable {
    private String field;
    private String type;
    private boolean isnull;
    private boolean prikey;
}
