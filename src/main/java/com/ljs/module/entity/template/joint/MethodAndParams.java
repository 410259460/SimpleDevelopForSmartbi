package com.ljs.module.entity.template.joint;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;


@Data
@AllArgsConstructor
public class MethodAndParams {
    private Method method;
    private Object[] params;
}
