package com.ljs.module.entity.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeNode {
    private String key;
    private String name;
    private boolean isFile;
    private String path;
    private List<TreeNode> children;

    public TreeNode(String key, String name, boolean isFile, String path) {
        this.key = key;
        this.name = name;
        this.isFile = isFile;
        this.path = path;
    }

    public boolean getIsFile() {
        return isFile;
    }

    public void setIsFile(boolean file) {
        isFile = file;
    }
}
