package com.ljs.module.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by lijunsai on 2021/08/05
 */
@Getter
@AllArgsConstructor
public enum ResponseStatus {
    ACTION_SUCCESS(2000, "操作成功"),
    ACTION_FAIL(5000, "操作失败")
    ;
    private Integer code;
    private String des;
}
