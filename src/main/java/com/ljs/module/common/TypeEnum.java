package com.ljs.module.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.File;

/**
 * 构建器枚举
 * Created by lijunsai on 2021/07/10
 */
@Getter
@AllArgsConstructor
public enum TypeEnum {
    // INPUT 和 NO_DEFAULT_VALUE_INPUT 共用 input.js模板 使用字段havaInit区分
    INPUT("有默认值的输入框","systemconfig" + File.separator + "input.js"),
    NO_DEFAULT_VALUE_INPUT("没有默认值的输入框","systemconfig" + File.separator + "input.js"),
    SELECT("下拉框","systemconfig" + File.separator + "select.js"),
    JUST_BUTTON("按钮","systemconfig" + File.separator + "button.js"),
    TEXT_AREA("文本域","systemconfig" + File.separator + "textarea.js"),
    RADIO("单选","systemconfig" + File.separator + "radio.js"),

    CUSTOM_TEMPLATE("自定义模板","custom" + File.separator),
    JOINT_TEMPLATE("联合模板","jointTemplate" + File.separator),

    PostTask("资源升级类","upgrade" + File.separator + "postupgrade.js"),
    UpgradeTask("升级类","upgrade" + File.separator + "upgrade.js");



    public String toString() {
        return this.name();
    }
    private String desc;
    private String template;
}
