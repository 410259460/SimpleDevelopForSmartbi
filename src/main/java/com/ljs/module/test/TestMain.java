package com.ljs.module.test;

public class TestMain {

    public static void main(String[] args) {
        // 按理说不同对象调用调用initFields出来的个数固定都是 9 个 或 1个
        // 在一个对象new 了数组之后 其他线程进来
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> new Son1().initFields()).start();
            new Thread(() -> new Son2().initFields()).start();
        }
    }
}
