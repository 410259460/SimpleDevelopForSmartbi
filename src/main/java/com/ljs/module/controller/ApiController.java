package com.ljs.module.controller;

import com.ljs.module.common.DataBean;
import com.ljs.module.entity.api.ApiEntityDto;
import com.ljs.module.util.ApiUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * api控制器  搜方法/搜类
 * Created by lijunsai on 2021/10/08
 */
@Controller
@RequestMapping("/api")
public class ApiController {
    @ResponseBody
    @PostMapping("/searchClass")
    public DataBean search(String word, String configs[], Integer page, Integer column){
        ApiUtil.column = column;
        Integer type = null;
        Boolean bool = false;
        for (String config : configs) {
            if (config.equals("js")) {
                type = 0;
                if (bool) {
                    type = null;
                    break;
                }
                bool = true;
            }
            if (config.equals("java")) {
                type = 1;
                if (bool) {
                    type = null;
                    break;
                }
                bool = true;
            }
        }
        List<ApiEntityDto> search = new ArrayList<>();
        Map<String, Object> ret = new HashMap<>();
        if (StringUtils.isBlank(word)) {
            if(type == null) {
                search = ApiUtil.allEntities;
            } else if (type == 1) {
                search = ApiUtil.transApiEntities2Dto(ApiUtil.javaEntities);
            } else if (type == 0) {
                search = ApiUtil.transApiEntities2Dto(ApiUtil.jsEntities);
            }
        } else {
            search = ApiUtil.search(word, type);
        }
        ret.put("count", search.size());
        ret.put("data", ApiUtil.page(search, page));
        return DataBean.success().setData(ret);
    }

    @ResponseBody
    @PostMapping("/searchMethod")
    public DataBean searchMethod(String word, String path, Integer type, Integer page, Integer column){
        ApiUtil.column = column;
        List<ApiEntityDto> search = ApiUtil.getMethod(path, type);
        Map<String, Object> ret = new HashMap<>();
        if (!StringUtils.isBlank(word)) {
            Set<ApiEntityDto> sortedSet = new TreeSet<>(Comparator.comparing(ApiEntityDto::getScore).reversed());
            for (ApiEntityDto apiEntityDto : search) {
                ApiUtil.search(apiEntityDto, word, sortedSet);
            }
            search = sortedSet.stream().collect(Collectors.toList());
        }
        ret.put("count", search.size());
        ret.put("data", ApiUtil.page(search, page));
        return DataBean.success().setData(ret);
    }
}
