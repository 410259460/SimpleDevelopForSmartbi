package com.ljs.module.controller;

import com.ljs.module.common.DataBean;
import com.ljs.module.entity.template.upgrade.Upgrade;
import com.ljs.module.util.UpgradeUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 升级类控制器
 * Created by lijunsai on 2021/07/22
 */
@Controller
@RequestMapping("/upgrade")
public class BuildUpgradeController {

    /**
     * 构建升级类
     * @param path 扩展包路径
     * @param type 构建类型
     * @param configs 勾选项
     * @param files 文件
     * @return 操作结果
     */
    @ResponseBody
    @PostMapping("/upgrade")
    public DataBean<String> buildUpgrade(String path, String type, String module, String[] configs, @RequestParam(name = "file", required = false) MultipartFile[] files, String extendJson){
        try {
            Upgrade upgrade = new Upgrade();
            upgrade.setPath(path).setType(type).setModule(module).setConfigs(configs).setFiles(files).setExtendJson(extendJson);
            return DataBean.success().setDesc(upgrade.build());
        } catch (Exception ex) {
            return DataBean.fail().setDesc(ex.getMessage());
        }
    }

    /**
     * 构建升级类
     * @return 操作结果
     */
    @ResponseBody
    @PostMapping("/upgrade/table")
    public DataBean<String> buildUpgradeWithTable(@RequestBody Upgrade upgradeFileBuild){
        try {
            return DataBean.success().setDesc(upgradeFileBuild.build());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return DataBean.success().setDesc("");
    }

    /**
     * 获取指定类型的当前升级类版本列表
     * @param path 扩展包路径
     * @param type 类型 升级类 资源升级类
     * @return
     */
    @ResponseBody
    @PostMapping("/getVersions")
    public List<String> getVersions(String path, String type, String module) throws Exception {
        UpgradeUtil.changeModule(path, module);
        List<String> nowVersions = new Upgrade().setType(type).getNowVersions(UpgradeUtil.basicDir);
        return nowVersions.stream().map(x -> {
            if (x.equals("0_0_0")){
                return Upgrade.task.get(type) + "_New";
            }
            return Upgrade.task.get(type) + "_" + x;
        }).collect(Collectors.toList());
    }

    /**
     * 输入扩展包路径之后会先调用getModules 读取到多个Module的包路径  后续的操作直接执行changeModule切换Module即可拿到包路径
     * @param path 扩展包
     * @return
     */
    @ResponseBody
    @PostMapping("/getModules")
    public DataBean<List<String>> getModules(String path){
        try {
            UpgradeUtil.init(path);
        } catch (Exception e) {
            return DataBean.fail("在application中没有找到注册Module的Bean");
        }
        UpgradeUtil.modulePathMap.entrySet();
        return DataBean.success(UpgradeUtil.modulePathMap.keySet().stream().collect(Collectors.toList()));
    }
}
